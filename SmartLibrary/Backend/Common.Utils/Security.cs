﻿using System;

namespace Common.Utils
{


	public class Security
    {
		public enum HashType
		{
			MD5,
			SHA1,
			SHA256,
			SHA384,
			SHA512
		}

		public static string GetChecksum(HashType hashType, string filePath)
		{
			using (var hasher = System.Security.Cryptography.HashAlgorithm.Create(hashType.ToString()))
			{
				using (var stream = System.IO.File.OpenRead(filePath))
				{
					var hash = hasher.ComputeHash(stream);
					return BitConverter.ToString(hash).Replace("-", "");
				}
			}
		}
	}
}
