﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SmartLibrary.Api.Model.Input
{
    public class SearchRequest
    {
        public string Query { get; set; }
        public decimal? SearchWeight { get; set; }

        public string[] GetWords()
        {
            return Regex.Matches(Query.ToLower(), @"\w+")
                        .OfType<Match>()
                        .GroupBy(x => x.Value)
                        .Select(x => x.Key)
                        .ToArray();
        }
    }
}
