﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Entities.Interfaces
{
    public interface IDocument
    {
        Task<string> GetContentAsync();
        Task<IDictionary<string, long>> GetWordFrequencyAsync();

    }
}
