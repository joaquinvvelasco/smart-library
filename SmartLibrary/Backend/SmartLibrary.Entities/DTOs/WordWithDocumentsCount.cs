﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Entities.DTOs
{
    public class WordWithDocumentsCount
    {
        public long Id { get; set; }
        public string WordName { get; set; }
        public long DocumentsCount { get; set; }
    }
}
