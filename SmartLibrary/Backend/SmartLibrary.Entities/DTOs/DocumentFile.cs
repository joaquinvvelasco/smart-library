﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Entities.DTOs
{
    public class DocumentFile
    {
        public string FileName { get; set; }
        public MemoryStream MemoryStream { get; set; }

        public DocumentFile(string fileName, byte[] content)
        {
            FileName = fileName;
            MemoryStream = new MemoryStream(content);
        }
    }
}
