﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Entities.DTOs
{
    public class DocumentRanked
    {
        public long IdDocument { get; set; }
        public string DocumentName { get; set; }
        public string FileName { get; set; }
        public double Rank { get; set; }
    }
}
