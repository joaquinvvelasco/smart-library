﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Entities.DTOs
{
    public class WordByDocumentDto
    {
        public long IdDocument { get; set; }
        public string DocumentName { get; set; }
        public string FileName { get; set; }
        public long DocumentsCount { get; set; }
        public long WordFrecuency { get; set; }

        public double CalculateRank(long totalDocumentsCount)
        {
            return WordFrecuency * Math.Log(totalDocumentsCount / DocumentsCount);
        }
    }
}
