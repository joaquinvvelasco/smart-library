﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using SmartLibrary.Entities.Abstract;
using System.Text;

namespace SmartLibrary.Entities.Domain
{
    public class PdfDocument : Document
    {

        public PdfDocument(string name, string fileName, string filePath):base(name, fileName, filePath)
        {

        }

        public PdfDocument()
        {

        }


        protected override string GetContentWithoutValidation()
        {
            StringBuilder text = new StringBuilder();
            using (PdfReader reader = new PdfReader(FilePath))
            {
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                }
            }
            return text.ToString();
        }
    }
}
