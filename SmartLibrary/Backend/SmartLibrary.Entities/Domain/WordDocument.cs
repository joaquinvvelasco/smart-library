﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Document = SmartLibrary.Entities.Abstract.Document;

namespace SmartLibrary.Entities.Domain
{
    public class WordDocument : Document
    {
        public WordDocument(string name, string fileName, string filePath) : base(name, fileName, filePath)
        {

        }

        public WordDocument()
        {

        }

        protected override string GetContentWithoutValidation()
        {
            using (WordprocessingDocument wordDocument = WordprocessingDocument.Open(FilePath, false))
            {
                Body body = wordDocument.MainDocumentPart.Document.Body;
                return body.InnerText.ToString();
            }
        }

    }
}
