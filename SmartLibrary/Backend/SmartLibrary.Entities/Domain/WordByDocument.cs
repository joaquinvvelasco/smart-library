﻿using Common.DataAcess.Sql;
using SmartLibrary.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Entities.Domain
{
    public class WordByDocument : Entity
    {
        [ForeignKey("Document")]
        public long IdDocument { get; set; }
        [ForeignKey("Word")]
        public long IdWord { get; set; }
        public Word  Word { get; set; }
        public Document Document{ get; set; }
        public long WordFrecuency { get; set; }

        public WordByDocument()
        {

        }

        public WordByDocument(string word, long wordFrecuency) : this(new Word(word), wordFrecuency)
        {
        }

        public WordByDocument(Word word, long wordFrecuency)
        {
            Word = word;
            WordFrecuency = wordFrecuency;
        }

    }
}
