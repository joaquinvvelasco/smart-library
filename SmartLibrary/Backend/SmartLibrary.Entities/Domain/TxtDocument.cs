﻿using SmartLibrary.Entities.Abstract;

namespace SmartLibrary.Entities.Domain
{
    public class TxtDocument : Document
    {
        public TxtDocument(string name, string fileName, string filePath) : base(name, fileName, filePath)
        {

        }

        public TxtDocument()
        {

        }
    }
}
