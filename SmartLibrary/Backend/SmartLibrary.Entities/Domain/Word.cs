﻿using Common.DataAcess.Sql;
using SmartLibrary.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Entities.Domain
{
    public class Word : Entity
    {
        public string WordName { get; set; }
        public ICollection<WordByDocument> Documents { get; set; }

        public Word()
        {

        }

        public Word(string wordName)
        {
            WordName = wordName;
        }
    }
}
