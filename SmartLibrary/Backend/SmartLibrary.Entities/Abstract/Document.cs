﻿using Common.DataAcess.Sql;
using Common.Utils;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using SmartLibrary.Entities.Abstract;
using SmartLibrary.Entities.Domain;
using SmartLibrary.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SmartLibrary.Entities.Abstract
{
    public class Document : Entity, IDocument
    {
        public string Checksum { get; private set; }
        public string DocumentName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public ICollection<WordByDocument> Words { get; set; }


        public Document() { }


        public static Document Create(string name, string fileName, string filePath)
        {
            switch(Path.GetExtension(name))
            {
                case ".pdf": return new PdfDocument(name, fileName, filePath);
                case ".docx": return new WordDocument(name, fileName, filePath);
                case ".doc": return new WordDocument(name, fileName, filePath);
                default: return new TxtDocument(name, fileName, filePath);
            }
        }

        protected Document(string name, string fileName, string filePath)
        {
            DocumentName = name;
            FileName = fileName;
            FilePath = filePath;
            Checksum = Security.GetChecksum(Security.HashType.SHA512, filePath);
        }

        protected void ValidateDocumentExists()
        {
            if (!File.Exists(FilePath))
                throw new ApplicationException("El documento no existe");
        }

        protected virtual string GetContentWithoutValidation()
        {
            string content = File.ReadAllText(FilePath);
            return content;
        }

        public string GetContent()
        {
            ValidateDocumentExists();
            return GetContentWithoutValidation();
        }

        public async Task<string> GetContentAsync()
        {
            return await Task.FromResult(GetContent());
        }

        private IDictionary<string, long> GetWordFrequency()
        {
            return Regex.Matches(GetContent().ToLower(), @"\w+")
                .OfType<Match>()
                .GroupBy(x => x.Value)
                .ToDictionary(x => x.Key, x => x.LongCount());
        }

        public async Task<IDictionary<string, long>> GetWordFrequencyAsync()
        {
            return await Task.FromResult(GetWordFrequency());
        }

        public void Process(IEnumerable<Word> words)
        {
            IDictionary<string, long> wordFrecuency = GetWordFrequency();
            IDictionary<string, Word> wordDictionary = words.ToDictionary(w => w.WordName, w => w);
            Words = new List<WordByDocument>();
            foreach(var wordInfo in wordFrecuency)
            {
                WordByDocument wordByDocument;
                if (wordDictionary.ContainsKey(wordInfo.Key))
                    wordByDocument = new WordByDocument(wordDictionary[wordInfo.Key], wordInfo.Value);
                else
                    wordByDocument = new WordByDocument(wordInfo.Key, wordInfo.Value);
                Words.Add(wordByDocument);
            }
        }
    }
}
