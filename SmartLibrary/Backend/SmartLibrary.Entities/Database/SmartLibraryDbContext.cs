﻿using Microsoft.EntityFrameworkCore;
using SmartLibrary.Entities.Abstract;
using SmartLibrary.Entities.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Entities.Database
{
    public class SmartLibraryDbContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Word>()
                .HasIndex(p => p.WordName)
                .IsUnique();
        }

        public SmartLibraryDbContext(DbContextOptions<SmartLibraryDbContext> options) : base(options)
        {
        }

        public DbSet<Word> Words { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<WordByDocument> WordsByDocuments { get; set; }

    }
}
