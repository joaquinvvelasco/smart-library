﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Common.DataAcess.Sql
{
    public class Parameter<T>
    {
        public int Page { get; set; }
        public int Top { get; set; }
        public Expression<Func<T, bool>> Where { get; set; }
        public Func<IQueryable<T>, IOrderedQueryable<T>> OrderBy { get; set; }
        public Func<IQueryable<T>, IIncludableQueryable<T, object>> Includes { get; set; }

        public Parameter()
        {
            Page = 0;
            Top = int.MaxValue;
            Where = null;
            OrderBy = null;
        }

        public Parameter(int page, int top)
        {
            Page = page;
            Top = top;
            Where = null;
            OrderBy = null;
        }


    }
}
