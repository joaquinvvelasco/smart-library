﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DataAcess.Sql
{
    public interface IRepository<T, K> where T : Entity where K : DbContext
    {
        Task<IEnumerable<T>> GetAll();
        Task Insert(T instances);
        Task Insert(ICollection<T> instances);
        Task Update(T instance);
        Task UpdateAll(T instance);
        Task Delete(T instance);
        Task Delete(ICollection<T> instances);
        Task<IEnumerable<T>> GetWithParameters(Parameter<T> parameters, bool asNoTracking = false);
        Task<T> GetById(long id);
        Task<long> Count();
    }
}
