﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DataAcess.Sql
{
    /// <summary>
    /// Clase que implementa las funcionalidades de un CRUD para una entidad de clase T.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Repository<T, K> : IRepository<T, K> where T : Entity, new() where K : DbContext
    {
        protected readonly K _dbContext;
        public Repository(K dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Update(T instance)
        {
            _dbContext.Set<T>().Update(instance);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAll(T instance)
        {
            _dbContext.Entry(_dbContext.Set<T>().FirstOrDefault(i => i.Id == instance.Id)).CurrentValues.SetValues(instance);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(T instance)
        {
            _dbContext.Set<T>().Remove(instance);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(ICollection<T> instances)
        {
            _dbContext.Set<T>().RemoveRange(instances);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Insert(T instance)
        {
            _dbContext.Add<T>(instance);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Insert(ICollection<T> instances)
        {
            _dbContext.AddRange(instances);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetWithParameters(Parameter<T> parameters, bool asNoTracking = false)
        {
            IQueryable<T> query = _dbContext.Set<T>();

            if (asNoTracking)
                query.AsNoTracking();

            if (parameters.Includes != null)
                query = parameters.Includes(query);

            if (parameters.Where != null)
                query = query.Where(parameters.Where);

            if (parameters.OrderBy != null)
                query = parameters.OrderBy(query);

            if (parameters.Page != 0)
                return await query.Skip((parameters.Page - 1) * parameters.Top).Take(parameters.Top).ToListAsync();
            else
                return await query.ToListAsync();
        }

        public async Task<T> GetById(long id)
        {
            return await _dbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public async Task<long> Count()
        {
            return await _dbContext.Set<T>().CountAsync();
        }
    }
}
