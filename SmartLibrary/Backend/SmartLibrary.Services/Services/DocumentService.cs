﻿using Common.DataAcess.Sql;
using Microsoft.AspNetCore.Http;
using SmartLibrary.Entities.Abstract;
using SmartLibrary.Entities.Database;
using SmartLibrary.Entities.Domain;
using SmartLibrary.Entities.DTOs;
using SmartLibrary.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Services.Services
{
    public class DocumentService
    {
        private readonly FileProviderService _fileProviderService;
        private readonly IRepository<Document, SmartLibraryDbContext> _documentRepository;
        private readonly IRepository<Word, SmartLibraryDbContext> _wordRepository;

        public DocumentService(FileProviderService fileProviderService, 
               IRepository<Document, SmartLibraryDbContext> documentRepository,
               IRepository<Word, SmartLibraryDbContext> wordRepository)
        {
            _fileProviderService = fileProviderService;
            _documentRepository = documentRepository;
            _wordRepository = wordRepository;
        }

        public async Task Create(IFormFile formFile)
        {
            var words = _wordRepository.GetAll().Result;
            string fileName = _fileProviderService.Upload(formFile);
            Document document = Document.Create(formFile.FileName, fileName, _fileProviderService.GetFilePath(fileName));
            ValidateDocument(document);
            document.Process(words);
            await _documentRepository.Insert(document);
        }

        public void ValidateDocument(Document newDocument)
        {
            Parameter<Document> parameter = new Parameter<Document>();
            parameter.Where = d => d.Checksum == newDocument.Checksum;
            var documents = _documentRepository.GetWithParameters(parameter);
            if (documents.Result.Any())
                throw new ApplicationException("El documento adjuntado ya existe en la base de datos");
        }

        public async Task<DocumentFile> GetDocumentFile(long id)
        {
            Document document = await _documentRepository.GetById(id);
            if (document == null)
                throw new ApplicationException("Documento no encontrado");
            byte[] content = _fileProviderService.Download(document.FileName);
            return new DocumentFile(document.DocumentName, content);
        }
            

    }
}
