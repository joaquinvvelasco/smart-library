﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Services.Services
{
    public class FileProviderService
    {
        private readonly string directoryPath;

        public FileProviderService(IConfiguration configuration)
        {
            directoryPath = configuration.GetSection("AppConfig")["DirectoryPath"];
            if (string.IsNullOrEmpty(directoryPath))
                throw new ApplicationException("El directorio de documentos no se encuentra configurado");
        }

        public string Upload(IFormFile file)
        {
            if (file == null || file.Length == 0)
                throw new ApplicationException("No se recibió el archivo adjuntado");
            string guid = Guid.NewGuid().ToString();
            try
            {
                using (var stream = new FileStream(GetFilePath(guid), FileMode.Create))
                {
                    stream.Position = 0;
                    file.CopyTo(stream);
                }
            }
            catch (Exception)
            {
                throw new ApplicationException("Ocurrió un error al momento de cargar el arhivo");
            }
            return guid;
        }

        public string GetFilePath(string instanceId)
        {
            return Path.Combine(directoryPath, instanceId);
        }

        public byte[] Download(string instanceId)
        {
            string filePath = GetFilePath(instanceId);
            if (!File.Exists(filePath))
                throw new ApplicationException("No existe el archivo al cual intenta acceder");
            try
            {
                return File.ReadAllBytes(filePath);
            }
            catch (Exception)
            {
                throw new ApplicationException(string.Format("Ocurrió un error al momento de leer el archivo {0}", instanceId));
            }
        }

        public void Delete(string instanceId)
        {
            string filePath = GetFilePath(instanceId);
            if (File.Exists(filePath))
            {
                try
                {
                    File.Delete(filePath);
                }
                catch (Exception)
                {
                    throw new ApplicationException("Ocurrio un error al momento de eliminar un archivo");
                }
            }

        }
    }
}
