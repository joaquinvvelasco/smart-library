﻿using Common.DataAcess.Sql;
using SmartLibrary.Api.Model.Input;
using SmartLibrary.DataAccess;
using SmartLibrary.Entities.Abstract;
using SmartLibrary.Entities.Database;
using SmartLibrary.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Services.Services
{
    public class SearchService
    {
        private readonly WordRepository _wordRepository;
        private readonly IRepository<Document, SmartLibraryDbContext> _documentRepository;
        private readonly WordByDocumentRepository _wordByDocumentRepository;

        private const int R = 5;
    
        public SearchService(WordRepository wordRepository,
            IRepository<Document, SmartLibraryDbContext> documentRepository,
            WordByDocumentRepository wordByDocumentRepository)
        {
            _wordRepository = wordRepository;
            _documentRepository = documentRepository;
            _wordByDocumentRepository = wordByDocumentRepository;
        }

        public IList<DocumentRanked> Search(SearchRequest searchRequest)
        {
            IList<DocumentRanked> documents = new List<DocumentRanked>(); 
            long documentsCount = _documentRepository.Count().Result;
            long[] relevantWordsIds = PickRelevantWords(searchRequest, documentsCount);
            
            if(relevantWordsIds != null && relevantWordsIds.Any())
            {
                IEnumerable<WordByDocumentDto> relevantDocuments = _wordByDocumentRepository.GetRelevantDocuments(relevantWordsIds, R);
                documents = relevantDocuments.GroupBy(g => g.IdDocument)
                                 .Select(s => new DocumentRanked
                                 {
                                     IdDocument = s.First().IdDocument,
                                     DocumentName = s.First().DocumentName,
                                     FileName = s.First().FileName,
                                     Rank = s.Sum(d => d.CalculateRank(documentsCount))
                                 })
                                 .OrderByDescending(o => o.Rank)
                                 .ToList();
            }
            return documents;
        }

        private long[] PickRelevantWords(SearchRequest searchRequest, long documentsCount)
        {
            long[] relevantWords = null;
            string[] words = searchRequest.GetWords();
            searchRequest.SearchWeight = searchRequest.SearchWeight ?? 100;
            decimal relativeSearchWeight = searchRequest.SearchWeight.GetValueOrDefault() / 100;
            var wordsWithDocumentsCount = _wordRepository.GetWordsWithDocumentsCount(words).OrderBy(w => w.DocumentsCount);
            if(wordsWithDocumentsCount.Any())
            {
                var minDocumentsCount = wordsWithDocumentsCount.FirstOrDefault().DocumentsCount;
                IEnumerable<WordWithDocumentsCount> wordsWithDocumentsCountFiltered = wordsWithDocumentsCount.Where(w => w.DocumentsCount <= ((documentsCount - minDocumentsCount) * relativeSearchWeight + minDocumentsCount));
                relevantWords = wordsWithDocumentsCountFiltered.Select(s => s.Id).ToArray();
            }
            return relevantWords;
        }
    }
}
