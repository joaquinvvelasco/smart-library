﻿using Common.Api.Controller;
using Common.Api.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SmartLibrary.Api.Model.Input;
using SmartLibrary.Entities.DTOs;
using SmartLibrary.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartLibrary.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SearchController : BaseController
    {
        private readonly SearchService _searchService;

        public SearchController(SearchService searchService)
        {
            _searchService = searchService;
        }


        
        [HttpPost]
        public IActionResult Search([FromBody] SearchRequest searchRequest)
        {
            if (searchRequest == null)
                return DoBadRequest("Parámetro de busqueda");
            return Ok(CustomResponse<IList<DocumentRanked>>.DoSuccess(_searchService.Search(searchRequest)));
        }
            

    }
}
