﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Api.Controller;
using Common.Api.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SmartLibrary.Services.Services;

namespace SmartLibrary.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class DocumentController : BaseController
    {
        private readonly DocumentService _documentService;

        public DocumentController(DocumentService documentService)
        {
            _documentService = documentService;
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile document)
        {
            if (document == null)
                return DoBadRequest("Documento");

            await _documentService.Create(document);
            var success = CustomResponse.DoSuccess("Documento creado correctamente");

            return Ok(success);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Download(long? id)
        {
            if (!id.HasValue)
                return DoBadRequest("Id");

            var documentFile = await _documentService.GetDocumentFile(id.GetValueOrDefault());
            return File(documentFile.MemoryStream, "APPLICATION/octet-stream", documentFile.FileName);
        }
    }
}
