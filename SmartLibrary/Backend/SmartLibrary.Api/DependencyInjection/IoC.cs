﻿using Common.DataAcess.Sql;
using Microsoft.Extensions.DependencyInjection;
using SmartLibrary.DataAccess;
using SmartLibrary.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartLibrary.Api.DependencyInjection
{
    public class IoC
    {
        public static void AddDependencies(IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddScoped(typeof(DocumentService));
            services.AddScoped(typeof(FileProviderService));
            services.AddScoped(typeof(SearchService));
            services.AddScoped(typeof(WordRepository));
            services.AddScoped(typeof(WordByDocumentRepository));

        }

    }
}
