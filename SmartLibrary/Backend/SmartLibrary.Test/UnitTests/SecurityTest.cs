﻿using Common.Utils;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartLibrary.Test.UnitTests
{
    public class SecurityTest
    {
        [Test]
        public void TestSameChecksum()
        {
            string content = "Esto es una prueba";
            string basePath = Path.GetTempPath();

            string fileName1 = Path.Combine(basePath, "Nombre1.txt");
            string fileName2 = Path.Combine(basePath, "Nombre2.txt");

            File.WriteAllText(fileName1, content);
            File.WriteAllText(fileName2, content);

            string checksum1 = Security.GetChecksum(Security.HashType.SHA256, fileName1);
            string checksum2 = Security.GetChecksum(Security.HashType.SHA256, fileName2);

            Assert.AreEqual(checksum1, checksum2);

            File.Delete(fileName1);
            File.Delete(fileName2);
        }

    }
}
