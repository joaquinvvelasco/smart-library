using NUnit.Framework;
using SmartLibrary.Entities;
using SmartLibrary.Entities.Domain;
using System.Collections;
using System.IO;

namespace SmartLibrary.Test
{
    public class DocumentTest
    {

        [Test]
        public void TestGetTxtContent()
        {
            string expected = "Esto es una prueba!";
            string fileName = Path.Combine(Path.GetTempPath(), "test.txt");
            File.WriteAllText(fileName, expected);
            
            TxtDocument document = new TxtDocument
            {
                FilePath = fileName
            };
            string obtained = document.GetContentAsync().Result;
            Assert.AreEqual(expected, obtained);

            File.Delete(fileName);
        }

        [Test]
        public void TestGetPdfContent()
        {
            string filePath = @"C:\Users\Joaquin\Desktop\UTN\Quinto\Practicas supervisadas\Listo\Regularidad\Informe final.pdf";
            PdfDocument document = new PdfDocument
            {
                FilePath = filePath
            };
            string obtained = document.GetContentAsync().Result;
        }

        [Test]
        public void TestGetWordContent()
        {
            string filePath = @"C:\Users\Joaquin\Desktop\UTN\Quinto\Practicas supervisadas\Informe final.docx";
            WordDocument document = new WordDocument
            {
                FilePath = filePath
            };
            string obtained = document.GetContentAsync().Result;
        }

        //[Test]
        //public void TestGetWordFrecuencyHarrt()
        //{
        //    TxtDocument document = new TxtDocument
        //    {
        //        FileName = @"C:\Users\Joaquin\Desktop\Joaquin\Documentos\Documento de prueba\test.txt"
        //    };

        //    var wordFrecuency = document.GetWordFrequencyAsync().Result;

        //}

        [Test]
        public void TestGetWordFrecuency()
        {
            string expected = "Esto esto es una prueba! con �sto es prueba";
            string filePath = Path.Combine(Path.GetTempPath(), "test.txt");
            File.WriteAllText(filePath, expected);

            TxtDocument document = new TxtDocument
            {
                FilePath = filePath
            };

            var wordFrecuency = document.GetWordFrequencyAsync().Result;

            Assert.AreEqual(wordFrecuency["esto"], 2);
            Assert.AreEqual(wordFrecuency["�sto"], 1);
            Assert.AreEqual(wordFrecuency["es"], 2);
            Assert.AreEqual(wordFrecuency["una"], 1);


            File.Delete(filePath);
        }


        [Test]
        public void TestProcess()
        {
            TxtDocument document = new TxtDocument
            {
                FilePath = @"C:\Users\Joaquin\Desktop\Joaquin\Documentos\Documento de prueba\test.txt"
            };

            //document.Process();

        }
    }
}