﻿using Common.Api.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Filters
{
    /// <summary>
    /// Filtro de excepciones para la API, el cual permite administrar los diferentes tipos de 
    /// excepciones: excepción de negocio, excepción por acceso no autorizado y excepción no controlada
    /// </summary>
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext contexto)
        {
            string message = null;
            //EXCEPCIÓN DE NEGOCIO
            if (contexto.Exception is ApplicationException)
            {
                ApplicationException ex = contexto.Exception as ApplicationException;
                message = ex.Message;
                // Por defecto el codigo HTTP es un 200, por lo que no modifico el codigo en caso de ser un error de negocio
            }
            //EXCEPCIÓN POR ACCESO NO AUTORIZADO
            else if (contexto.Exception is UnauthorizedAccessException)
            {
                message = "Acceso no autorizado.";
            }
            //EXCEPCIÓN NO CONTROLADA
            else
            {
                //TODO: Hacer logs de excepciones no controladas en archivos planos o tabla de logs de base de datos
                //para facilitar el debug de errores.
                message = "Ha ocurrido un error.";
            }

            CustomResponse response = CustomResponse.DoFailed(message);

            contexto.Result = new JsonResult(response);

            base.OnException(contexto);
        }
    }
}
