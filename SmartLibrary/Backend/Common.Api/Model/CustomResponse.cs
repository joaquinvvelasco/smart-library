﻿namespace Common.Api.Model
{
    public class CustomResponse<T> : CustomResponse
    {
        public T Data { get; set; }
        private CustomResponse(string message, bool success, T data) : base(message, success)
        {
            this.Data = data;
        }

        private static CustomResponse<T> Create(string message, bool success, T data)
        {
            return new CustomResponse<T>(message, success, data);
        }

        public static CustomResponse<T> DoSuccess(string message, T data)
        {
            return Create(message, true, data);
        }

        public static CustomResponse<T> DoSuccess(T data)
        {
            return DoSuccess(string.Empty, data);
        }

        public static CustomResponse<T> DoFailed(T data)
        {
            return DoFailed(string.Empty, data);
        }

        public static CustomResponse<T> DoFailed(string message, T data)
        {
            return Create(message, false, data);
        }
    }

    public class CustomResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }

        private static CustomResponse Create(string message, bool success)
        {
            return new CustomResponse(message, success);
        }

        public static CustomResponse DoSuccess(string message)
        {
            return Create(message, true);
        }

        public static CustomResponse DoFailed(string message)
        {
            return Create(message, false);
        }

        protected CustomResponse(string message, bool success)
        {
            this.Success = success;
            this.Message = message;
        }
    }


}
