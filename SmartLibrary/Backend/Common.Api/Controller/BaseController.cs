﻿using Common.Api.Filters;
using Common.Api.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Api.Controller
{
    [ApiExceptionFilter]
    public abstract class BaseController : ControllerBase
    {
        public IActionResult DoBadRequest(string objectRequestName)
        {
            return BadRequest(CustomResponse.DoFailed(string.Format("No se recibió {0}", objectRequestName)));
        }
    }
}
