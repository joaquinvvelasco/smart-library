﻿using Common.DataAcess.Sql;
using SmartLibrary.Entities.Database;
using SmartLibrary.Entities.Domain;
using SmartLibrary.Entities.DTOs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;


namespace SmartLibrary.DataAccess
{
    public class WordByDocumentRepository : Repository<WordByDocument, SmartLibraryDbContext>
    {
        public WordByDocumentRepository(SmartLibraryDbContext smartLibraryDbContext) : base(smartLibraryDbContext)
        {
        }

        public IEnumerable<WordByDocumentDto> GetRelevantDocuments(long[] wordIds, int r)
        {
            var a = _dbContext.WordsByDocuments.Where(w => wordIds.Contains(w.IdWord))
                            .Select(s => s.IdWord).Distinct()
                            .SelectMany(idWord => _dbContext.WordsByDocuments.Where(w => w.IdWord == idWord).OrderByDescending(o => o.WordFrecuency).Take(5))
                            .Select(s => new WordByDocumentDto
                            {
                                IdDocument = s.Document.Id,
                                DocumentName = s.Document.DocumentName,
                                FileName = s.Document.FileName,
                                DocumentsCount = s.Word.Documents.Count(),
                                WordFrecuency = s.WordFrecuency
                            }).ToList();
            return a;
        }

    }
}
