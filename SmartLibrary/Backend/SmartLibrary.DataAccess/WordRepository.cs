﻿using Common.DataAcess.Sql;
using SmartLibrary.Entities.Database;
using SmartLibrary.Entities.Domain;
using SmartLibrary.Entities.DTOs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SmartLibrary.DataAccess
{
    public class WordRepository : Repository<Word, SmartLibraryDbContext>
    {
        public WordRepository(SmartLibraryDbContext smartLibraryDbContext) : base(smartLibraryDbContext)
        {
        }

        public IEnumerable<WordWithDocumentsCount> GetWordsWithDocumentsCount(string[] words)
        {
            return _dbContext.Words.Where(w => words.Contains(w.WordName))
                            .Select(w => new WordWithDocumentsCount
                            {
                                Id = w.Id,
                                WordName = w.WordName,
                                DocumentsCount = w.Documents.Count
                            });
        }

    }
}
