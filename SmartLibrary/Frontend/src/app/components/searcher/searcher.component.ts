import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Document } from 'src/app/models/response/Document';
import { SearchRequest } from 'src/app/models/request/SearchRequest';

@Component({
  selector: 'app-searcher',
  templateUrl: './searcher.component.html',
  styleUrls: ['./searcher.component.css']
})
export class SearcherComponent implements OnInit {
  documentList: Document[] = [
    new Document(123, "Este es un ejemplo"),
    new Document(123, "Este es un ejemplo")
  ];

  form : FormGroup;

  constructor(private fb : FormBuilder) {
    this.form = this.fb.group({
      query : ['', Validators.required]
    })
   }

  ngOnInit(): void {
  }

  search(){
    let searchRequest: SearchRequest = new SearchRequest(this.form.get("query")?.value);

    console.log(searchRequest);
  }

}
