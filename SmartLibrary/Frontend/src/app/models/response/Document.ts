export class Document
{
    id: number;
    documentName: string;

    constructor(id : number, documentName: string)
    {
        this.id = id;
        this.documentName = documentName;
    }

}