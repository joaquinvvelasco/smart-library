export class SearchRequest
{
    query: string;

    constructor(query : string){
        this.query = query;
    }
}